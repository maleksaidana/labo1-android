package com.example.etudiant;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class update extends AppCompatActivity {

    private String username;
    private EditText user;
    private EditText psw;
    private dbworker db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        user = findViewById(R.id.idd);
        psw  = findViewById(R.id.mdp);
        Bundle   b = getIntent().getExtras();
        username=b.getString("mesage");
        user.setText(username);
        db =new dbworker();
    }

    public void update(View view) {

           db.execute(user.getText().toString().trim(),psw.getText().toString());
    }


    public class dbworker extends AsyncTask {


        @Override
        protected void onPostExecute(Object o) {




                Intent Facteur = new Intent(update.this,login.class);
                Facteur.putExtra("mesage",(String)o);
                startActivity(Facteur);


        }

        @Override
        protected Object doInBackground(Object[] param) {
            URL url;

            try {
                url = new URL("http://10.30.12.100:4000/andro/update");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                String msg = URLEncoder.encode("user", "utf-8")
                        + "=" + URLEncoder.encode((String) param[0], "utf-8") + "&"
                        + URLEncoder.encode("psw", "utf-8") + "="
                        + URLEncoder.encode((String) param[1], "utf-8") ;

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();


                while ((line = bufr.readLine()) != null) {
                    sbuff.append(line + "\n");


                }
                return sbuff.toString();
            }
            catch(Exception ex){
                return ex.getMessage();
            }
        }
    }

}
