package com.example.etudiant;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class liste extends AppCompatActivity {


    public ArrayList<String> listNom;
    public ArrayAdapter<String> listAdapter;
    private ListView list1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste);


        inintArrayList();
        initArrayAdapter();

        list1 = (ListView)findViewById(R.id.list1);
        list1.setAdapter(listAdapter);

        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"You just selected: " + ((TextView)view).getText().toString(),Toast.LENGTH_LONG).show();
            }
        });

        list1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                listNom.remove(position);
                listAdapter.notifyDataSetChanged();
                return  true;
            }
        });

        dbworker db=new dbworker();
        db.execute();


    }

    private void inintArrayList()
    {
        listNom = new ArrayList<String>();
       listNom.add("Liste Des Activitées: ");

    }

    private void initArrayAdapter()
    {
        listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listNom);

    }

    public class dbworker extends AsyncTask {


        public dbworker(){

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Object o) {



            for (String l: (ArrayList<String>) o ) {
                listNom.add(l);
                listAdapter.notifyDataSetChanged();
            }


        }

        @Override
        protected Object doInBackground(Object[] objects) {
            URL url;
            ArrayList<String> ll=new ArrayList<String>();

            try {
                url = new URL("http://192.168.1.5:4000/andro/activites");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));


                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while ((line = bufr.readLine()) != null) {
                    sbuff.append(line + "\n");
                    ll.add(line);
                    listAdapter.notifyDataSetChanged();

                }
                return ll;
            }
            catch(Exception ex){
                return ex.getMessage();
            }
        }
    }

}


